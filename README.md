# ffmpeg-wrapper #

A simple wrapper around ffmpeg for Node.js. Includes binaries for Windows (win32), OSX (darwin) and CentOS (linux -- other distros may be supported)

### How to use ###

    var ffmpeg = require("ffmpeg-wrapper");
    
    // encode in.mov to out.mp4 with h.264 video, AAC audio, and the YADIF filter
    // overwrite the output file if it already exists (default is true)
    var encoder = ffmpeg({
    	input: "in.mov",
    	output: "out.mp4",
    	overwrite: true,
    	params: {
    		"vf": "yadif",
    		"vcodec": "libx264",
    		"acodec": "libvo_aacenc",
    		"map": ["0:0", "0:1"]
    	},
    	complete: function(error, complete) {
    		console.log("ENCODING COMPLETE", error, complete);
    	},
    	progress: function(progress) {
    		console.log("PROGRESS", progress);
    	}
    });
    
    // get info about input file
    encoder.probe(function(error, mediaInfo) {
    	if(!error)
    		console.log(mediaInfo); // from ffprobe
    });
    
    // start the encoding
    encoder.start();
 
    // stop the encoder after 20 seconds
    setTimeout(function() {
        encoder.stop();
    }, 20000);
    