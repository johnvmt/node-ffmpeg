module.exports = function(config) {
	return new FfmpegEncoder(config);
};

var path = require('path');
var childProcess = require('child_process');
var Utils = require(path.join(__dirname, "Utils.js"));
var configDefaults = require(path.join(__dirname, "defaults.js"));

function FfmpegEncoder(config) {
	this.setConfig(config);
}

FfmpegEncoder.prototype.setConfig = function(config) {
	this.config = Utils.objectMerge(configDefaults, config);
};

FfmpegEncoder.prototype.getConfig = function() {
	return this.config;
};

FfmpegEncoder.prototype.probe = function(callback) {
	childProcess.exec(this.config.bins.ffprobe + " -v quiet -print_format json -show_format -show_streams " + this.config.input, function(error, stdout, stderr) {
		if(error)
			callback(error, null);
		else {
			try {
				var mediaInfo = JSON.parse(stdout);
				callback(null, mediaInfo);
			}
			catch(error) {
				callback("parse_error", null);
			}
		}
	});
};

FfmpegEncoder.prototype.start = function() {
	var self = this;

	var cmd = self.ffmpegCmd(self.config);
	if(self.config.debug)
		console.log("FFMPEG PARAMS: " + cmd);
	var ffmpegProcess = childProcess.spawn(self.config.bins.ffmpeg, cmd.split(' '));

	// ffmpeg outputs to stderr
	ffmpegProcess.stderr.on('data', function(data) {
		if(self.config.debug)
			console.log("STDERR OUTPUT: ", data.toString());
		if (typeof self.config.progress === "function") {
			var progress = self.parseProgress(data);
			if (progress)
				self.config.progress(progress);
		}
	});

	ffmpegProcess.on('exit', function (exitCode) {
		if(typeof self.config.complete === "function") {
			if(exitCode == 0)
				self.config.complete(null, true);
			else
				self.config.complete(exitCode, null);
		}

		// stop() function was called
		if(typeof self.stopCallback === "function") {
			self.stopCallback(null, true);
			delete self.stopCallback;
		}
	});

	self.ffmpegProcess = ffmpegProcess;
};

FfmpegEncoder.prototype.stop = function(callback) {
	if(typeof callback === "function")
		this.stopCallback = callback;
	this.ffmpegProcess.kill();
};

FfmpegEncoder.prototype.parseProgress = function(progressStr) {
	var self = this;
	var matches = String(progressStr).trim().replace(/=\s*/ig, "=").split(" ");
	var progress = {};

	matches.forEach(function(match) {
		var param = match.split("=");
		if(typeof param[1] !== "undefined" && self.config.progressKeys.indexOf(param[0]) >= 0) // filter to keys
			progress[param[0]] = param[1];
	});

	if(Object.keys(progress).length > 1)
		return progress;
	else
		return null;
};

FfmpegEncoder.prototype.ffmpegCmd = function(config) {
	var cmd = "";
	if(typeof config.input === "string")
		cmd += "-i " + config.input;

	// either overwrite without asking or fail immediately if file exists
	if(typeof config.overwrite === "undefined" || config.overwrite)
		cmd += " -y";
	else
		cmd += " -n";

	if(typeof config.params === "string")
		cmd += " "+ config.params;
	else if(typeof config.params === "object") {
		Utils.objectForEach(config.params, function(param, key) {
			if(Array.isArray(param)) {
				param.forEach(function(value) {
					cmd += " -" + key + " " + value;
				});
			}
			else
				cmd += " -" + key + " " + param;
		});
	}

	if(typeof config.output === "string")
		cmd += " " + config.output;

	return cmd;
};
